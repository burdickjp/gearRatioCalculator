# gearRatioCalculator
a Jupyter notebook for calculating, graphing, and working with gear ratios

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/burdickjp%2FgearRatioCalculator/master?labpath=index.ipynb)
